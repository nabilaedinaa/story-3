from django.shortcuts import render
from django.shortcuts import redirect
from .models import Friends, ClassYear
from . import forms


# Create your views here.
def home(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')

def education(request):
    return render(request, 'education.html')

def experience(request):
    return render(request, 'experience.html')

def food(request):
    return render(request, 'food.html')

def travel(request):
    return render(request, 'travel.html')

def seefriend(request):
    friends = Friends.objects.all()
    classyear = ClassYear.objects.all()
    response = {'friends' : friends, 'classyear': classyear}
    return render(request, 'seefriend.html', response)

def addfriend(request):
    if request.method == 'POST':
        form = forms.FriendForm(request.POST)
        print(1)
        if form.is_valid():
            print(2)
            form.save()
            return redirect('homepage:seefriend')
    else:
        form = forms.FriendForm()
    return render(request, 'addfriend.html', {'form': form})

def deletefriend(request, name):
    friends = Friends.objects.get(name=name)
    friends.delete()
    return redirect('/seefriend/')



