from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('education/', views.education, name='education'),
    path('experience/', views.experience, name='experience'),
    path('food/', views.food, name='food'),
    path('travel/', views.travel, name='travel'),
    path('addfriend/', views.addfriend, name='addfriend'),
    path('seefriend/', views.seefriend, name='seefriend'),
    path('seefriend/deletefriend/<str:name>', views.deletefriend, name='deletefriend'),
]